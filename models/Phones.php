<?php

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "phones".
 *
 * @property int $id
 * @property int $number phone number
 * @property string $user_id
 *
 * @property Users $user
 */
class Phones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'user_id'], 'required'],
            [['number'], 'string', 'max' => 32],
            [['user_id'], 'string', 'max' => 32],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'phone number',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\UsersQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\PhonesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PhonesQuery(get_called_class());
    }

    public static function petty($phone = '')
    {
        // https://github.com/hflabs/dadata-php

        $token = Yii::$app->params['DData_api_token'];
        $secret = Yii::$app->params['DData_api_secret'];
        $cache =Yii::$app->cache;
        $key ='numbers'.$phone ;
        $result = $cache->get($key);
        if ($result === false) {
            if (empty($token) || empty($secret))
                throw new \Exception('need DData keys');
            $dadata = new \Dadata\DadataClient($token, $secret);
            $result = $dadata->clean("phone", $phone);
            $cache->set($key, $result);
        }

        return $result;
    }
}
