<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $id id
 * @property string $name name
 * @property int $age
 * @property string $city
 * @property int $membership_date
 *
 * @property Phones[] $phones
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'age', 'city', 'membership_date'], 'required'],
            ['membership_date', 'string', 'max' => 255],
            [['age'], 'integer'],
            [['id'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 500],
            [['city'], 'string', 'max' => 1000],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'age' => 'Age',
            'city' => 'City',
            'membership_date' => 'Membership Date',
        ];
    }

    /**
     * Gets query for [[Phones]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\PhonesQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phones::className(), ['user_id' => 'id']);
    }
    public static function petty($fio = '')
    {
        // https://github.com/hflabs/dadata-php

        $token = Yii::$app->params['DData_api_token'];
        $secret = Yii::$app->params['DData_api_secret'];
        $cache =Yii::$app->cache;
        $key ='numbers'.$fio ;
        $result = $cache->get($key);
        if ($result === false) {
            if (empty($token) || empty($secret))
                throw new \Exception('need DData keys');
            $dadata = new \Dadata\DadataClient($token, $secret);
            $result = $dadata->clean("name", $fio);
            $cache->set($key, $result);
        }

        return $result;
    }
    /**
     * {@inheritdoc}
     * @return \app\models\query\UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\UsersQuery(get_called_class());
    }
}
