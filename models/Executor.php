<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\ArrayHelper;


class Executor extends Model
{
    public $xml;
    public $_path;
    public $parsed;
    const FILE = '@app/client.xml';


    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            ['_path', 'validatePath', 'skipOnEmpty' => false],
            //  ['xml', 'required'],
            ['xml', 'validateXml', 'skipOnEmpty' => false],

        ];
    }

    public function validateXml($attribute, $params)
    {
        try {
            $this->loadfromfile();
            $this->parsed = simplexml_load_string($this->xml);
            //  $this->parsedarray = @json_decode(@json_encode($this->parsed), 1);
        } catch (Exception $e) {
            $this->addError($attribute, 'Incorrect input  data ' . $e->getMessage());
        }

    }

    public function loadfromfile()
    {
        return $this->xml = file_get_contents($this->_path);
    }

    public function validatePath($attribute, $params)
    {
        if (is_file($this->_path = Yii::getAlias(self::FILE)) && is_readable($this->_path) && $this->_path = realpath($this->_path))
            $this->loadfromfile();
        else
            $this->addError($attribute, 'Incorrect input  data ' . $this->_path);
    }

    public function choseNeed()
    {
        foreach ($this->parsed->children() as $index => $item) {
            $users [$id = $item[0]['id'] . ''] = [
                'id' => $id,
                'membership_date' => $item->membership_date . '',
                'name' => $item->name . '',
                'age' => $item->age . '',
                'city' => $item->city . '',
                'numbers' => $item->numbers->number,
            ];
        }
        return $users;
    }

    public function process()
    {
        /*@todo  add  try */
        $arr = $this->choseNeed();/*забираем тоьлко нужные поля из  перечисленного списка*/
        $transaction_id = Yii::$app->db->beginTransaction();
        foreach ($arr as $index => $item) {
            $numbers = ArrayHelper::remove($item, 'numbers');
            /** @var Users $user */
            $user = Users::find()->where(['id' => $item['id']])->one();
            if (!$user) $user = new Users($item);
            $user->setAttributes($item);
            $saved[$user->id] = $user->save();
            if ($saved[$user->id] && $numbers) {
                if (!$user->isNewRecord) {
                    Phones::deleteAll(['user_id' => $user->id]);
                }
                foreach ($numbers as $i => $number) {
                    $phone = new  Phones(['number'=>$n = $number->__toString(), 'user_id' => $user->id]);
                    $phone_saved[$user->id][$n] = $phone->save();
                }
            }


        }
        return [$transaction_id->commit(), $saved, $phone_saved];

    }

}
