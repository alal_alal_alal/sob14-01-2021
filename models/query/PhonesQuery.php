<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Phones]].
 *
 * @see \app\models\Phones
 */
class PhonesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\Phones[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Phones|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
