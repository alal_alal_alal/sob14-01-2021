<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'телефоны';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h2><?= Html::encode($this->title) ?></h2>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'number',
            ['label' => 'Petty', 'format' => 'raw', 'value' => function ($model) {
                return '<pre class="hljs js">' . json_encode(\app\models\Phones::petty($model->number), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            }],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
