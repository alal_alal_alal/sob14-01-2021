<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->source;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

                'source:raw:Исходное ФИО одной строкой',
              'result:raw:Стандартизованное ФИО одной строкой',
                'result_genitive:raw:ФИО в родительном падеже (кого?)',
                'result_dative:raw:ФИО в дательном падеже (кому?)',
                'result_ablative:raw:ФИО в творительном падеже (кем?)',
               'surname:raw:Фамилия',
                'name:raw:Имя',
                'patronymic:raw:Отчество',
                'gender:raw:Пол',
                'qc:raw:Код проверки',
        ],
    ]) ?>

</div>
