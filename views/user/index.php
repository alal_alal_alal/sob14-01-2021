<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.5.0/build/highlight.min.js');
$this->registerJsFile('https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.5.0/build/languages/javascript.min.js');
$this->registerCssFile('https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.5.0/build/styles/default.min.css');
$this->registerCssFile('https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10.5.0/build/styles/idea.min.css');
?>

    <script src=""></script>

    <div class="users-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
            <a href="javascript:void();" id="start_tour" class="btn btn-info"><span
                        class="glyphicon glyphicon-question-sign"></span></a>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                ['attribute' => 'name', 'format' => 'raw', 'value' => function ($model, $key) {
                    return $model->name . '<a data-toggle="modal" href="' . $u = Url::to(['getpettyname', 'id' => $model->id, 'ajax' => true]) . '"  data-remote="' . $u . '" data-target="#Modal"  class="pull-right"><span class="glyphicon glyphicon-modal-window"></span></a>';
                }],
                'age',
                'city',
                'membership_date',

                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{modal}', 'buttons' => ['modal' => function ($url, $model, $key) {
                    return '<a data-toggle="modal" href="' . $u = Url::to(['view', 'id' => $model->id, 'ajax' => true]) . '"  data-remote="' . $u . '" data-target="#Modal"><span class="glyphicon glyphicon-modal-window"></span></a>';
                }]],
            ],
        ]); ?>


    </div>
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
<?php
$script = new \yii\web\JsExpression(/** @lang JavaScript */ "
$('#Modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);debugger;
  modal.find('.modal-title').text(button.closest('tr').find('td').eq('2').text())
  modal.find('.modal-body').text('loading');
  modal.find('.modal-body').load(button.attr('href'),()=>{
document.querySelectorAll('.hljs').forEach(block => {
            hljs.highlightBlock(block);
        });
    })
});

");
$this->registerJs($script);

?>

<?= \luc\tourist\Tourist::widget(
    ['clientOptions' => [ //Bootstrap Tour Options, see: http://bootstraptour.com/api/
        'steps' => [
            [
                'element' => "#w0 > table > thead > tr:nth-child(1) > th:nth-child(2)",
                'title' => "Step 1",
                'content' => "сортировка",
            ],
            [
                'element' => "#w0-filters > td:nth-child(2) > input",
                'title' => "Step 2",
                'content' => "фильтры",
            ],
            [
                'element' => "#w0 > table > tbody > tr:nth-child(1) > td:nth-child(3) > a > span",
                'title' => "Step 3",
                'content' => "стандартизация загруженных  ФИО клиентов (откроется в модальном окне)",
            ],
            [
                'element' => "#w0 > table > tbody > tr:nth-child(3) > td:nth-child(7) > a:nth-child(1) > span",
                'title' => "Step 4",
                'content' => "открыть <u>на новой странице</u> <strong>номерателефонов</strong>   этого клиента",
            ],
            [
                'element' => "#w0 > table > tbody > tr:nth-child(3) > td:nth-child(7) > a:nth-child(2) > span",
                'title' => "Step 5",
                'content' => "стандартизация загруженных  <strong>номеров телефонов клиентов</strong> (откроется в <strong>модальном окне</strong>)",
            ],
        ],
    ]]);
$this->registerJs(new \yii\web\JsExpression(/** @lang JavaScript */ 'document.querySelector("#start_tour").onclick =  function(event, el){
tour.restart();
event.preventDefault();
}'));
?>